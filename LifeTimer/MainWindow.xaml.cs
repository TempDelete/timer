﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace LifeTimer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //public static DateTime deathTime = new DateTime(2101, 5, 7, 12, 3, 35, 47);
        public static DateTime deathTime = new DateTime(2101, 5, 7, 12, 3, 35, 47);
        DispatcherTimer timer = new DispatcherTimer();
        TimeSpan period;
        public MainWindow()
        {
            InitializeComponent();
            timer.Tick += new EventHandler(delegate {
                ShowTimer();
            });
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
        }
        void ShowTimer()
        {
            period = (deathTime - DateTime.Now);
            label1_Copy.Content = "дней: " + period.Days;
            label2_Copy.Content = "часов: " + period.Hours;
            label3_Copy.Content = "минут: " + period.Minutes;
            label4_Copy.Content = "секунд: " + period.Seconds;
        }
    }
    public class NtpClient
    {
        public static DateTime GetNetworkTime()
        {
            return GetNetworkTime("time.windows.com"); // time-a.nist.gov
        }
        public static DateTime GetNetworkTime(string ntpServer)
        {
            IPAddress[] address = Dns.GetHostEntry(ntpServer).AddressList;

            if (address == null || address.Length == 0)
                throw new ArgumentException("Could not resolve ip address from '" + ntpServer + "'.", "ntpServer");

            IPEndPoint ep = new IPEndPoint(address[0], 123);

            return GetNetworkTime(ep);
        }
        public static DateTime GetNetworkTime(IPEndPoint ep)
        {
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            try
            {
                s.Connect(ep);

                byte[] ntpData = new byte[48]; // RFC 2030 
                ntpData[0] = 0x1B;
                for (int i = 1; i < 48; i++)
                    ntpData[i] = 0;

                s.Send(ntpData);
                s.Receive(ntpData);

                byte offsetTransmitTime = 40;
                ulong intpart = 0;
                ulong fractpart = 0;

                for (int i = 0; i <= 3; i++)
                    intpart = 256 * intpart + ntpData[offsetTransmitTime + i];

                for (int i = 4; i <= 7; i++)
                    fractpart = 256 * fractpart + ntpData[offsetTransmitTime + i];

                ulong milliseconds = (intpart * 1000 + (fractpart * 1000) / 0x100000000L);
                s.Close();

                TimeSpan timeSpan = TimeSpan.FromTicks((long)milliseconds * TimeSpan.TicksPerMillisecond);

                DateTime dateTime = new DateTime(1900, 1, 1);
                dateTime += timeSpan;

                TimeSpan offsetAmount = TimeZone.CurrentTimeZone.GetUtcOffset(dateTime);
                DateTime networkDateTime = (dateTime + offsetAmount);
                return networkDateTime;
            }
            catch
            {
                return DateTime.Now;
            }
        }
    }
}
